package com.digitalonus.movie.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.digitalonus.movie.data.model.Movie
import com.digitalonus.movie.data.repository.MoviesRepository
import com.digitalonus.movie.data.state.DataState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MoviesViewModel @ViewModelInject constructor(
    private val repository: MoviesRepository
    ): ViewModel() {

    private val _movies: MutableLiveData<DataState<List<Movie>>> = MutableLiveData()

    val movies: LiveData<DataState<List<Movie>>>
        get() = _movies

    fun setMoviesEvent(apiKey: String) {
        viewModelScope.launch {
            repository.searchMovies(apiKey = apiKey)
                .collect {
                    it?.let {
                        _movies.value = it
                    }
                }
        }
    }

}