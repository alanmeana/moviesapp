package com.digitalonus.movie.data.repository

import com.digitalonus.movie.data.model.Movie
import com.digitalonus.movie.data.net.MovieMapper
import com.digitalonus.movie.data.net.MovieService
import com.digitalonus.movie.data.state.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


open class MoviesRepositoryImpl @Inject constructor(
        private val service: MovieService,
        private val mapper: MovieMapper): MoviesRepository {

    override suspend fun searchMovies(apiKey: String): Flow<DataState<List<Movie>>?> = flow {
        emit(DataState.Loading)
        try {
            val response = service.searchMovies(api_key = apiKey)
            if (response.isSuccessful) {
                response.body()?.results?.let {
                    emit(DataState.Success(data = mapper.mapFromEntityList(entities = response.body()?.results!!)))
                }
            } else {
                emit(DataState.CodeError(code = response.code()))
            }
        } catch (e: Exception) {
            emit(DataState.Error(exception = e))
        }
    }.flowOn(Dispatchers.IO)

}