package com.digitalonus.movie.data.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MovieEntity (

    @SerializedName("id")
    @Expose
    var id: Int = 0,

    @SerializedName("adult")
    @Expose
    var adult: Boolean = false,

    @SerializedName("backdrop_path")
    @Expose
    var backdropPath: String? = "",

    @SerializedName("genre_ids")
    @Expose
    var genreIds: List<Int>? = null,

    @SerializedName("original_language")
    @Expose
    var originalLanguage: String = "",

    @SerializedName("original_title")
    @Expose
    var originalTitle: String = "",

    @SerializedName("overview")
    @Expose
    var overview: String = "",

    @SerializedName("popularity")
    @Expose
    var popularity: Float = 0F,
    @SerializedName("poster_path")
    @Expose
    var posterPath: String = "",

    @SerializedName("release_date")
    @Expose
    var releaseDate: String = "",

    @SerializedName("title")
    @Expose
    var title: String = "",

    @SerializedName("video")
    @Expose
    var video: Boolean = false,

    @SerializedName("vote_average")
    @Expose
    var voteAverage: Float = 0F,

    @SerializedName("vote_count")
    @Expose
    var voteCount: Float = 0F

)