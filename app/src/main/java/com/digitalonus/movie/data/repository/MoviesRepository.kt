package com.digitalonus.movie.data.repository

import com.digitalonus.movie.data.model.Movie
import com.digitalonus.movie.data.state.DataState
import kotlinx.coroutines.flow.Flow

interface MoviesRepository {

    suspend fun searchMovies(apiKey: String): Flow<DataState<List<Movie>>?>

}