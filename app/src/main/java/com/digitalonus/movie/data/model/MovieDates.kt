package com.digitalonus.movie.data.model

data class MovieDates (

    var maximum: String = "",
    var minimum: String = ""

)