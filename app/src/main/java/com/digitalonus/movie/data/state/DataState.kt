package com.digitalonus.movie.data.state

sealed class DataState<out R> {

    data class Success<out T>(val data: T): DataState<T>()
    data class Error(val exception: Exception): DataState<Nothing>()
    data class CodeError(val code: Int): DataState<Nothing>()
    object Loading: DataState<Nothing>()

}