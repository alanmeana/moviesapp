package com.digitalonus.movie.data.net

import com.digitalonus.movie.data.model.Movie
import com.digitalonus.movie.domain.EntityMapper
import javax.inject.Inject

class MovieMapper @Inject constructor(): EntityMapper<MovieEntity, Movie> {

    override fun mapFromEntity(domainModel: Movie): MovieEntity =
        MovieEntity (
            id = domainModel.id,
            adult = domainModel.adult,
            backdropPath = domainModel.backdropPath,
            genreIds = domainModel.genreIds,
            originalLanguage = domainModel.originalLanguage,
            originalTitle = domainModel.originalTitle,
            overview = domainModel.overview,
            popularity = domainModel.popularity,
            posterPath = domainModel.posterPath,
            releaseDate = domainModel.releaseDate,
            title = domainModel.title,
            video = domainModel.video,
            voteAverage = domainModel.voteAverage,
            voteCount = domainModel.voteCount
        )

    override fun mapToEntity(entity: MovieEntity): Movie =
        Movie().apply {
            id = entity.id
            adult = entity.adult
            backdropPath = entity.backdropPath
            genreIds = entity.genreIds
            originalLanguage = entity.originalLanguage
            originalTitle = entity.originalTitle
            overview = entity.overview
            popularity = entity.popularity
            posterPath = entity.posterPath
            releaseDate = entity.releaseDate
            title = entity.title
            video = entity.video
            voteAverage = entity.voteAverage
            voteCount = entity.voteCount
    }

    fun mapFromEntityList(entities: List<MovieEntity>): List<Movie> = entities.map { mapToEntity(it) }

}