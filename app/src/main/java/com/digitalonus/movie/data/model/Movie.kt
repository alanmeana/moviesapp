package com.digitalonus.movie.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class Movie: Parcelable {

    var id: Int = 0
    var adult: Boolean = false
    var backdropPath: String? = ""
    var genreIds: List<Int>? = null
    var originalLanguage: String = ""
    var originalTitle: String = ""
    var overview: String = ""
    var popularity: Float = 0F
    var posterPath: String = ""
    var releaseDate: String = ""
    var title: String = ""
    var video: Boolean = false
    var voteAverage: Float = 0F
    var voteCount: Float = 0F

}

/*@Parcelize
data class Movie (

    var id: Int = 0,
    var adult: Boolean = false,
    var backdropPath: String? = "",
    var genreIds: List<Int>? = null,
    var originalLanguage: String = "",
    var originalTitle: String = "",
    var overview: String = "",
    var popularity: Float = 0F,
    var posterPath: String = "",
    var releaseDate: String = "",
    var title: String = "",
    var video: Boolean = false,
    var voteAverage: Float = 0F,
    var voteCount: Float = 0F

): Parcelable*/