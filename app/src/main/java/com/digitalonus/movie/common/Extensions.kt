package com.digitalonus.movie.common

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.commit

fun AppCompatActivity.replaceFragment(fragment: Fragment, tag: String? = null, container: Int, transition: Int = FragmentTransaction.TRANSIT_NONE, backStack: String? = null) {
    supportFragmentManager.commit {
        setTransition(transition)
        replace(container, fragment, tag)
        addToBackStack(backStack)
    }
}

fun AppCompatActivity.addFragment(fragment: Fragment, tag: String? = null, container: Int, transition: Int = FragmentTransaction.TRANSIT_NONE, backStack: String? = null) {
    supportFragmentManager.commit {
        setTransition(transition)
        add(container, fragment, tag)
        addToBackStack(backStack)
    }
}

fun Fragment.toastShort(text: String) {
    Toast.makeText(this.requireContext(), text, Toast.LENGTH_SHORT).show()
}

fun AppCompatActivity.toastShort(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

fun logE(tag: String = "MoviesApp", text: String) {
    Log.e(tag, text)
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}