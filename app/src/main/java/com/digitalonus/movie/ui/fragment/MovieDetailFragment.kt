package com.digitalonus.movie.ui.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalonus.movie.R
import com.digitalonus.movie.base.BaseFragment
import com.digitalonus.movie.data.model.Movie
import kotlinx.android.synthetic.main.fragment_movie_detail.*

class MovieDetailFragment: BaseFragment() {

    companion object {
        @JvmStatic
        fun newInstance() = MovieDetailFragment()
        val TAG = MovieDetailFragment::class.java.canonicalName
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_movie_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movie = arguments?.getParcelable<Movie>("Movie")
        movie?.let { m ->
            title.text = String.format("Title: ${m.title}")
            originalLanguage.text = String.format("Original Language: ${m.originalLanguage}")
            popularity.text = String.format("Popularity: ${m.popularity}")
            voteAverage.text = String.format("Vote Average: ${m.voteAverage}")
            overview.text = String.format("Overview: ${m.overview}")
        }

    }

}