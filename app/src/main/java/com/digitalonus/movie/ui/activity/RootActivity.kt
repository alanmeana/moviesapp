package com.digitalonus.movie.ui.activity

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.digitalonus.movie.R
import com.digitalonus.movie.base.BaseActivity
import com.digitalonus.movie.common.replaceFragment
import com.digitalonus.movie.data.model.Movie
import com.digitalonus.movie.ui.fragment.MovieDetailFragment
import com.digitalonus.movie.ui.fragment.MoviesListFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.toolbar_view.*

@AndroidEntryPoint
class RootActivity : BaseActivity(), MoviesListFragment.TouchActionDelegate {

    private val toolbar: Toolbar by lazy { toolbar_view_widget as Toolbar }

    override fun getToolbarInstance(): Toolbar = toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        val fragment = MoviesListFragment.newInstance()
        replaceFragment(fragment = fragment, tag = MoviesListFragment.TAG, container = R.id.fragment_container)
    }

    override fun openDetail(movie: Movie) {
        val fragment = MovieDetailFragment.newInstance()
        val bundle = Bundle().apply {
            putParcelable("Movie", movie)
        }
        fragment.arguments = bundle
        replaceFragment(fragment = fragment, tag = MovieDetailFragment.TAG, container = R.id.fragment_container)
    }

    override fun onBackPressed() {
        val frag = supportFragmentManager.findFragmentByTag(MoviesListFragment.TAG)
        if (frag != null && frag.isVisible) finish()
        else super.onBackPressed()
    }

}