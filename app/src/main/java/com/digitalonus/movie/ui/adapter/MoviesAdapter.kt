package com.digitalonus.movie.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalonus.movie.R
import com.digitalonus.movie.base.BaseRecyclerAdapter
import com.digitalonus.movie.common.Constants
import com.digitalonus.movie.data.model.Movie
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_item.view.*

class MoviesAdapter(movies: List<Movie>, private val callback: (Movie) -> Unit): BaseRecyclerAdapter<Movie>(masterList = movies.toMutableList()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder =
        MovieViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false))

    inner class MovieViewHolder(private val view: View): BaseViewHolder<Movie>(view = view) {
        override fun onBind(data: Movie, position: Int) {
            val movie: Movie = data
            with(view) {
                Picasso.get().load("${Constants.IMAGEURL}${movie.posterPath}").into(poster_view)
                poster_view.setOnClickListener { callback.invoke(movie) }
                movie_title.text = movie.title
            }
        }
    }

}