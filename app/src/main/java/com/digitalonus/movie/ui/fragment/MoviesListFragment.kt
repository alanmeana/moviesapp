package com.digitalonus.movie.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.digitalonus.movie.R
import com.digitalonus.movie.ui.adapter.MoviesAdapter
import com.digitalonus.movie.base.BaseFragment
import com.digitalonus.movie.common.Constants
import com.digitalonus.movie.common.gone
import com.digitalonus.movie.common.toastShort
import com.digitalonus.movie.common.visible
import com.digitalonus.movie.data.model.Movie
import com.digitalonus.movie.data.state.DataState
import com.digitalonus.movie.viewmodel.MoviesViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_movies_list.*

@AndroidEntryPoint
class MoviesListFragment: BaseFragment() {

    private var touchActionDelegate: TouchActionDelegate? = null
    private val viewModel: MoviesViewModel by viewModels()
    private lateinit var adapter: MoviesAdapter
    private var isListEmpty = true

    companion object {
        @JvmStatic
        fun newInstance() = MoviesListFragment()
        val TAG = MoviesListFragment::class.java.canonicalName
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context.let {
            if (it is TouchActionDelegate) touchActionDelegate = it
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_movies_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeObservers()
        viewModel.setMoviesEvent(apiKey = Constants.APIKEY)
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.setMoviesEvent(apiKey = Constants.APIKEY)
        }
    }

    private fun subscribeObservers() {
        viewModel.movies.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Success<List<Movie>> -> {
                    retry_message.gone()
                    displayLoading(false)
                    val lst = dataState.data
                    setAdapter(list = lst)
                }
                is DataState.Loading -> {
                    displayLoading(true)
                }
                is DataState.Error -> {
                    displayLoading(false)
                    displayError(message = dataState.exception.message)
                }
                is DataState.CodeError -> {
                    displayLoading(false)
                    val code = "${dataState.code}"
                    when  {
                        code.first() == '3' -> displayError(message = "error code $code")
                        code.first() == '4' -> displayError(message = "error code $code")
                        code.first() == '5' -> displayError(message = "error code $code")
                        else -> displayError(message = "Unknown Error")
                    }
                }
            }
        })
    }

    private fun setAdapter(list: List<Movie>) {
        list.run { isListEmpty = this.isEmpty() }
        adapter = MoviesAdapter(movies = mutableListOf(), callback = { mov ->
            touchActionDelegate?.openDetail(movie = mov)
        })
        recycler_view.apply {
            layoutManager = GridLayoutManager(requireContext(), 3)
            this.adapter = this@MoviesListFragment.adapter
        }
        adapter.updateList(list = list)
    }

    private fun displayLoading(isLoading: Boolean) {
        swipeRefreshLayout.isRefreshing = isLoading
    }

    private fun displayError(message: String?) {
        if (isListEmpty) retry_message.visible()
        message?.let {
            toastShort(text = message)
        } ?: run {
            toastShort(text = "Unknown error")
        }
    }

    interface TouchActionDelegate {
        fun openDetail(movie: Movie)
    }

}