package com.digitalonus.movie.domain

interface EntityMapper <Entity, DomainModel>{

    fun mapToEntity(entity: Entity): DomainModel

    fun mapFromEntity(domainModel: DomainModel): Entity
}