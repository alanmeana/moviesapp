package com.digitalonus.movie.di

import com.digitalonus.movie.data.net.MovieMapper
import com.digitalonus.movie.data.net.MovieService
import com.digitalonus.movie.data.repository.MoviesRepository
import com.digitalonus.movie.data.repository.MoviesRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideRepository(
        service: MovieService,
        movieMapper: MovieMapper
    ): MoviesRepository =
        MoviesRepositoryImpl(service, movieMapper)

}