package com.digitalonus.movie.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import java.util.HashSet

abstract class BaseRecyclerAdapter<T: Any>(private val masterList: MutableList<T> =  mutableListOf()): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    internal var selectedItems = HashSet<T>()
    private var recentlyDeleted = HashSet<T>()


    override fun getItemCount(): Int = masterList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as BaseViewHolder<T>).onBind(data = masterList[position], position = position)
    }

    abstract class BaseViewHolder<E>(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun onBind(data: E, position: Int)
    }

    internal fun getItemAtPosition(position: Int): T = masterList[position]

    internal fun getItems(): MutableList<T> = masterList

    internal fun updateList(list: List<T>) {
        masterList.clear()
        masterList.addAll(elements = list)
        notifyDataSetChanged()
    }

}