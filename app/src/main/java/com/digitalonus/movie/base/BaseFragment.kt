package com.digitalonus.movie.base

import androidx.fragment.app.Fragment

abstract class BaseFragment: Fragment()