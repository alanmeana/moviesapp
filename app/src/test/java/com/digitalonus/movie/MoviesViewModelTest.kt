package com.digitalonus.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.digitalonus.movie.data.model.Movie
import com.digitalonus.movie.data.repository.MoviesRepository
import com.digitalonus.movie.data.state.DataState
import com.digitalonus.movie.viewmodel.MoviesViewModel
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Maybe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Assert.*
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MoviesViewModelTest {

    private lateinit var viewModel: MoviesViewModel
    private val dispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var repository: MoviesRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(dispatcher)
        viewModel = MoviesViewModel(repository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        dispatcher.cleanupTestCoroutines()
    }

    @Test
    fun happyPath() {
        dispatcher.runBlockingTest {
            Mockito.`when`(repository.searchMovies(ArgumentMatchers.anyString())).thenAnswer {
                return@thenAnswer Maybe.just(ArgumentMatchers.anyList<Movie>())
            }
            val observer = mock(Observer::class.java) as Observer<LiveData<List<Movie>>>
            viewModel.movies.observeForever(observer)

            viewModel.setMoviesEvent(ArgumentMatchers.anyString())
            assertNotNull(viewModel.movies.value)
            assertEquals(DataState.Success, viewModel.movies.value)
        }
    }

}