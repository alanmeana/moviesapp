package com.digitalonus.movie

import com.digitalonus.movie.data.model.Movie
import com.digitalonus.movie.data.net.MovieService
import com.digitalonus.movie.viewmodel.MoviesViewModel
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Assert.*
import org.mockito.ArgumentMatchers

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ServiceTest {

    private val dispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var service: MovieService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        dispatcher.cleanupTestCoroutines()
    }

    @Test
    fun happyPath() {
        dispatcher.runBlockingTest {
            val movies = service.searchMovies(ArgumentMatchers.anyString())
            val actualRepo = movies.body()?.results
        }
    }

}